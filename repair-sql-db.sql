## Repair MS SQL DB. ** USE AT YOUR OWN RISK ** 
EXEC sp_resetstatus 'YOUR_DB_NAME';
ALTER DATABASE YOUR_DB_NAME SET EMERGENCY
DBCC checkdb('YOUR_DB_NAME')
ALTER DATABASE YOUR_DB_NAME SET SINGLE_USER WITH ROLLBACK
IMMEDIATE
DBCC CheckDB('YOUR_DB_NAME',REPAIR_ALLOW_DATA_LOSS)
ALTER DATABASE YOUR_DB_NAME SET MULTI_USER;
GO
