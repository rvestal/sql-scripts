# sql-scripts

My repository of SQL server scripts I've used in my career. Feel free to use them at no charge, but please give me credit for the work

* sqlversion.sql - Used to verify the version information of a server running MS SQL Server
* repair-sql-db.sql - Used to repair a corrupted or offline SQL DB based on transaction logs. ** This can potentially cause damage. Use at your own risk **